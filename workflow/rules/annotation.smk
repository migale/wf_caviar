rule prokka:
    input:
        contigs="results/assembly/{sample}/contigs.fasta",
        proteins=config["proteins"],
    output:
        multiext(
            "results/annot/prokka/{sample}/{sample}.",
            "fna",
            "gff",
            "faa",
            "ffn",
            "tbl",
            "fsa",
            "tsv",
            "txt",
            "gbk",
            "sqn",
        ),
    params:
        outdir=lambda wildcards, output: __import__("os").path.dirname(output[0]),
        genus=config["genus"],
        species=config["species"],
        extra="--gffver 3 --centre X --compliant --rnammer --gram '+' ",
    log:
        "results/annot/prokka/{sample}.log",
    threads: 4
    shell:
        """
        conda activate prokka-1.14.6
        prokka --force --cpus {threads} --outdir {params.outdir} --prefix {wildcards.sample} --locustag {wildcards.sample} {params.extra} --genus {params.genus} --species {params.species} --strain {wildcards.sample} --proteins {input.proteins} {input.contigs}
        conda deactivate
        """


rule eggnog:
    input:
        faa="results/annot/prokka/{sample}/{sample}.faa",
        db=config["eggnog_db"],
    output:
        multiext(
            "results/annot/eggnog/{sample}.emapper.",
            "annotations",
            "hits",
            "seed_orthologs",
        ),
    params:
        outdir=lambda wildcards, output: __import__("os").path.dirname(output[0]),
    log:
        "logs/eggnog/{sample}.log",
    threads: 4
    shell:
        """
        conda activate eggnog-mapper-2.1.8
        emapper.py --cpu {threads} -i {input.faa} --itype proteins --database bact -m diamond --data_dir {input.db} --output_dir {params.outdir} --output {wildcards.sample} >> {log} 2>&1
        conda deactivate
        """


rule bakta:
    input:
        contigs="results/assembly/{sample}/contigs.fasta",
        db=config["bakta_db"],
        # proteins=config["proteins"],
    output:
        multiext(
            "results/annot/bakta/{sample}/{sample}.",
            "tsv", 
            "gff3", 
            "gbff", 
            "embl", 
            "fna", 
            "ffn", 
            "faa", 
            "hypotheticals.tsv", 
            "hypotheticals.faa", 
            "json", 
            "txt", 
            "png", 
            "svg", 
        ),
    params:
        outdir=lambda wildcards, output: __import__("os").path.dirname(output[0]),
        genus=config["genus"],
        species=config["species"],
        extra="--compliant",
    log:
        "results/annot/bakta/{sample}.log",
    threads: 24
    shell:
        """
        conda activate bakta-1.9.1
        bakta --force --threads {threads} --db {input.db} --output {params.outdir} --prefix {wildcards.sample} --locus-tag {wildcards.sample} {params.extra} --genus {params.genus} --species {params.species} --strain {wildcards.sample} {input.contigs}
        conda deactivate
        """
