rule pool_fastq:
    input:
        lambda wildcards: __import__("glob").glob(
            "{raw_data}/{sequencing_folder}/*_{library}_*{R}.fastq.gz".format(
                **SAMPLES.loc[wildcards.sample], **wildcards, **config
            )
        ),
    output:
        "results/data/{sample}_{R}.fastq.gz",
    params:
        extra="--validate-seq",
    log:
        "logs/pool_fastq/{sample}_{R}.log",
    threads: 4
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq {input} --out-file {output} --threads {threads} {params.extra} >> {log} 2>&1
        conda deactivate 
        """


rule fastqc:
    input:
        "results/data/{sample}_{R}.fastq.gz",
    output:
        html="results/qc/fastqc/{sample}_{R}.html",
        zip="results/qc/fastqc/{sample}_{R}_fastqc.zip",
    params:
        "--quiet",
    log:
        "logs/fastqc/{sample}_{R}.log",
    threads: 1
    wrapper:
        "v1.25.0/bio/fastqc"


rule multiqc:
    input:
        expand(
            "results/qc/fastqc/{sample}_{R}_fastqc.zip", sample=SAMPLES.index, R=[1, 2]
        ),
        expand("report/fastp/{sample}_fastp.json", sample=SAMPLES.index),
        "results/quast/report.tsv",
        expand("results/annot/prokka/{sample}/{sample}.txt", sample=SAMPLES.index),
    output:
        "report/multiqc.html",
    params:
        use_input_files_only=True,
    log:
        "logs/multiqc.log",
    wrapper:
        "v3.3.3/bio/multiqc"


rule gene_detect:
    input:
        reads=expand("results/data/{{sample}}_{R}.fastq.gz", R=[1, 2]),
        gene=config["gene_detect"],
    output:
        bam="results/gene_detect/{sample}.bam",
        tsv="results/gene_detect/{sample}.txt",
    log:
        "logs/gene_detect/{sample}.log",
    threads: 4
    shell:
        """
        conda activate minimap2-2.22
        conda activate --stack samtools-1.14
        minimap2 -ax sr {input.gene} -t {threads} --secondary=no {input.reads} | \
        samtools sort --threads {threads} --output-fmt BAM -o {output.bam} -
        samtools index {output.bam}
        samtools quickcheck {output.bam}
        samtools flagstats --threads {threads} {output.bam} > {output.tsv}
        conda deactivate 
        conda deactivate 
        """
