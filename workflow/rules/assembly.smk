rule fastp:
    input:
        sample=expand("results/data/{{sample}}_{R}.fastq.gz", R=[1, 2]),
    output:
        trimmed=expand("results/trimmed/{{sample}}_{R}.fastq.gz", R=[1, 2]),
        html="report/fastp/{sample}.html",
        json="report/fastp/{sample}_fastp.json",
    log:
        "logs/fastp/{sample}.log",
    params:
        extra="--length_required 50",
    threads: 4
    wrapper:
        "v1.25.0/bio/fastp"


rule seqtk_subsample:
    input:
        f1="results/trimmed/{sample}_1.fastq.gz",
        f2="results/trimmed/{sample}_2.fastq.gz",
    output:
        f1="results/subset/{sample}_1.fastq.gz",
        f2="results/subset/{sample}_2.fastq.gz",
    params:
        n=config["subsample"],
        seed=12345,
    log:
        "logs/seqtk_subsample/{sample}.log",
    threads: 4
    wrapper:
        "v1.25.0/bio/seqtk/subsample/pe"


rule ribo:
    input:
        R1="results/subset/{sample}_1.fastq.gz",
        R2="results/subset/{sample}_2.fastq.gz",
        reference=config["reference"],
    output:
        contigs="results/ribo/{sample}/seed/final_long_reads/riboSeedContigs.fasta",
    params:
        mem_tot=100,
        outdir=lambda wildcards, output: __import__("os")
        .path.dirname(output.contigs)
        .split("seed")[0],
    log:
        "logs/ribo/{sample}.log",
    threads: 8
    shell:
        """
        conda activate riboSeed-0.4.90
        rm --dir --recursive {params.outdir}
        ribo run --config . --reference_fasta {input.reference} --fastq1 {input.R1} --fastq2 {input.R2} --output {params.outdir} --experiment_name {wildcards.sample} --cores {threads} --memory {params.mem_tot} >> {log} 2>&1 || mkdir --parents {params.outdir} && touch -a {output.contigs}
        conda deactivate
        """


rule ribo_stats:
    input:
        fastx=expand(
            "results/ribo/{sample}/seed/final_long_reads/riboSeedContigs.fasta",
            sample=SAMPLES.index,
        ),
    output:
        stats="results/ribo/ribo.tsv",
    log:
        "logs/ribo/ribo_stats.log",
    params:
        extra="--tabular",
    threads: 2
    wrapper:
        "v1.31.0/bio/seqkit/stats"


rule spades:
    input:
        R1="results/subset/{sample}_1.fastq.gz",
        R2="results/subset/{sample}_2.fastq.gz",
        trusted="results/ribo/{sample}/seed/final_long_reads/riboSeedContigs.fasta",
    output:
        contigs="results/assembly/{sample}/contigs.fasta",
    params:
        mem_tot=500,
        outdir=lambda wildcards, output: __import__("os").path.dirname(output.contigs),
        k=config["k_spades"],
    log:
        "logs/spades/{sample}.log",
    threads: 20
    shell:
        """
        conda activate spades-3.15.3
        spades.py --threads {threads} --memory {params.mem_tot} -o {params.outdir} --careful -k {params.k} -1 {input.R1} -2 {input.R2} --trusted-contigs {input.trusted} >> {log} 2>&1
        conda deactivate
        """


rule quast:
    input:
        fasta=expand("results/assembly/{sample}/contigs.fasta", sample=SAMPLES.index),
        ref=config["reference"],
    output:
        multiext("results/quast/report.", "html", "tex", "txt", "pdf", "tsv"),
        "results/quast/quast.log",
    threads: 4
    log:
        "logs/quast.log",
    params:
        extra="--min-contig 200",  #To be in confomity with --compliant option of Prokka
    wrapper:
        "v1.25.0/bio/quast"


localrules:
    ln_contigs,


rule ln_contigs:
    input:
        "results/assembly/{sample}/contigs.fasta",
    output:
        "results/assembly/pool/{sample}.fasta",
    log:
        "logs/ln_contigs/{sample}.log",
    shell:
        """
        ln --symbolic --relative --verbose {input} {output}  > {log}
        """


rule checkm:
    input:
        expand("results/assembly/pool/{sample}.fasta", sample=SAMPLES.index),
    output:
        "results/checkm/results.tsv",
    params:
        bin=lambda wildcards, input: __import__("os").path.dirname(input[0]),
        outdir=lambda wildcards, output: __import__("os").path.dirname(output[0]),
        genus=config["genus"],  # check `checkm taxon_list`
    log:
        "logs/checkm.log",
    threads: 4
    shell:
        """
        conda activate checkm-genome-1.1.3
        checkm taxonomy_wf --threads {threads} --extension fasta --tab_table --file {output} genus {params.genus} {params.bin} {params.outdir} >> {log} 2>&1
        conda deactivate
        """


rule drep:
    input:
        [
            expand("results/annot/prokka/{sample}/{sample}.fsa", sample=SAMPLES.index),
            config["reference"],
        ],
    output:
        "results/drep/data_tables/Ndb.csv",
        "results/drep/figures/Primary_clustering_dendrogram.pdf",
        "results/drep/figures/Secondary_clustering_MDS.pdf",
    params:
        outdir=lambda wildcards, output: __import__("os").path.dirname(
            __import__("os").path.dirname(output[0])
        ),
    log:
        "logs/drep.log",
    threads: 24
    shell:
        """
        conda activate drep-3.2.2
        dRep compare --genomes {input} --processors {threads} {params.outdir} >> {log} 2>&1
        conda deactivate
        """
