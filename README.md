# wf_caviar

Ce workflow [snakemake](https://snakemake.github.io/) a pour objectif d'assembler des génomes d'*Enterococcus cecorum* à partir des reads brutes et de données de références publiées.

Le graph est le suivant :

![](graph.png)

## Configuration 

Les principaux paramètres sont spécifiés dans le fichier [config/config.yaml](config/config.yaml). On y retrouve : 

| Variable    | Définition                                                                                                            | Par défaut                                                                          |
| ----------- | --------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| `samples`   | Table des échantillons à analyser avec deux colonnes `sample` et `library` (exemple : `ceco_comA_004_2` `lib593974` ) | `/work_home/cmidoux/GIT/wf_caviar/config/samples.tsv`                               |
| `raw_data`  | Path vers ces données                                                                                                 | `/work_projet/cecotype/2022-CAVIAR-SEQUENCING/RAW`                                  |
| `workdir`   | Path vers le dossier de travail ou seront produits les résultats                                                      | `/work_home/cmidoux/caviar`                                                         |
| `subsample` | Taille des sous-échantillonnages pour faciliter l'assemblage                                                          | `750000`                                                                            |
| `k_spades`  | Taille des k-mers utilisés par `spades`                                                                               | `21,33,55,77`                                                                       |
| `reference` | Path vers le génome de référence utilisé par `riboSeed`                                                               | `/work_projet/cecotype/REF/NCTC12421/NCTC12421.fasta `                              |
| `genus`     | Genre utilisé par `prokka` pour l'annotation                                                                          | `Enterococcus`                                                                      |
| `species`   | Espèce utilisée par `prokka` pour l'annotation                                                                        | `cecorum`                                                                           |
| `proteins`  | Path vers le catalogue de protéines utilisé par `prokka` pour l'annotation                                            | `/work_projet/cecotype/NANOPORE_ASSEMBLY-2020/Ref/Refseq/Enterococcus/proteins.faa` |
| `eggnog_db` | Path vers la database eggNOG                                                                                          | `/db/outils/eggnog-mapper/`                                                         |
| `kaiju_db`  | Path vers la database Kaiju                                                                                           | `/db/outils/kaiju-2021-03/nr_euk/`                                                  |

## Outputs

-   `report/multiqc.html` : MULTIQC report with :
    -   FASTQC raw data quality report
    -   FASTP trimming report
    -   QUAST assembly report
    -   PROKKA annotation report
-   `results/kaiju/krona.html` & `results/kaiju/kaiju.tsv` : Raw data taxonomic annotation.
-   `results/ribo/ribo.tsv` : riboSeed assembly metrics.
-   `results/assembly/{sample}/contigs.fasta` : Sample assembly after `fastp`, `seqtk_subsample`, `riboSeed` and `spades`.
-   `results/quast/report.html` : Genome assembly evaluation.
-   `results/checkm/results.tsv` : Assessment of genome quality (completeness and contamination) by `CheckM`.
-   `results/drep/` : Clustering of assembled genomes.
-   `results/annot/prokka/{sample}/{sample}.gbk` : Contig annotation by `prokka`.
-   `results/annot/eggnog/{sample}.emapper.hits` : Functional annotation by `eggNOG`.
-   `results/gene_detect/{sample}.txt` : Gene-specific alignment metrics.

## Exécution

Commandes à utiliser sur MIGALE pour exécuter le workflow :

```
# connection à un noeud de calcul
qlogin -pe thread 8

# activation environnement conda Snakemeake
conda activate snakemake-7.5.0

# execution à blanc
snakemake --printshellcmds --use-conda --dryrun

# Graph
snakemake --forceall --rulegraph  | dot -Tpng > graph.png

# execution réelle
snakemake --printshellcmds --cores 8 --use-conda --restart-times 5 --keep-going

# soumission de jobs
snakemake --printshellcmds --cores 8 --jobs 100 --use-conda --restart-times 1 --keep-going --cluster 'qsub -V -cwd -R y -N {rule} -pe thread {threads} -e logs/sge/ -o logs/sge/' --latency-wait 60

# le tout sur un job
mkdir -p logs/ && qsub -cwd -V -N caviar2 -pe thread 4 -e logs/ -o logs/ -q infinit.q -b y "conda activate snakemake-7.5.0 && snakemake --printshellcmds --jobs 500 --use-conda --restart-times 1 --keep-going --cluster 'qsub -V -cwd -R y -N {rule} -pe thread {threads} -e logs/sge/ -o logs/sge/' --latency-wait 60 --rerun-incomplete && conda deactivate"
```
